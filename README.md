# A Cleaning Schedule for Flatmates

## This Django App serves to make organizing and scheduling tasks in a shared flat easier:

The advantages to a classic paper calendar are:
- the individual tasks can be given different intervals. This is difficult to keep track of with a paper calender, where all tasks are usually rotated at the same time.
- it can be accessed from anywhere.
- it is fair. If a flatmate leaves for a while, there can arise conflict when they return and always resume the task with the lowest workload. The data base treats every flatmate the same. 
- tasks that only have to be done once or twice per year, like registerng for bulk waste or shuting down the outside water system in winter, are not forgotten.

Once set up, all flatmates can browse to the app where they find a scheduling table with an overview which tasks they are assigned to and when they are due.
Tasks that are done can be checkmarked, and then rotated to the next flatmate in line.

![Image:Overview of the Scheduler in the Browser](imgs/screen_schedule.png)

Each task can be given an different interval in which the assignee has to finish it.

If a flatmate is out of the house for some time, they can indicate so by clicking the "Not at home" button in the Flatmate table.
This way, they get skipped over in the rotation of the tasks.

![Image: Overview of the Flatmate table](imgs/screen_flatmate.png)

### Tutorial:

1. Create a virtualenvironment with py3.5 or py3.6:
    `virtualenv pyvenv -p python3`    
and activate it:
    `source pyvenv/bin/activate`

2. Clone this repo:
    `git clone flatschedule.git flatschedule`    
and cd into it:
    `cd flatschedule`    

3. Install Django:
     `pip install Django`
OR, if you set up a nginx+uwsgi-webserver:
    `pip install -r flatschedule/requirements.txt`

4. Adapt the settings.py file (i.e. creating a secret key):
    ```python
    python manage.py shell
    >> from django.core.management.utils import get_random_secret_key
    >> get_random_secret_key()
    ```    
    Replace the Secret key in the settings.py file with the string you just created.

 
5. Create the DB from the models:
   `python manage.py migrate`

6. If you haven't already, create a superuser and follow the prompts to set your username and password:
    `python manage.py createsuperuser`

7. Start the webserver via django:
     `pyhon manage.py runserver 8000`
OR set up nginx and uwsgi by following the uwsgi-tutorial 
(https://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html)
and creating the necessary uwsgi.ini and nginx.conf files for this project.

8. In your browser, go to:
    `localhost:8000/admin`
and login with your superuser credentials and create entries in the DB.

9. Then visit:
     `localhost:8000/schedule`
see who's turn it is to clean which room, click 'Done' when you cleaned a room,
click 'rotate', when it's the next persons turn.    

Keep the server running and tell your flatmates the link address.    
You only ever need to use the admin interface again when someone new moves in or you decide to change the cleaning interval for a task.
