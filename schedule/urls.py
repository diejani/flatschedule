from django.urls import path

from . import views

app_name = "schedule"
urlpatterns = [
    path('', views.index, name="index"),
    path("<int:room_id>/done", views.done, name="done"),
    path("<int:room_id>/rotate", views.rotate, name="rotate"),
    path("<int:c_id>/hold", views.hold, name="hold"),
    path("<int:c_id>/unhold", views.unhold, name="unhold"),
]
