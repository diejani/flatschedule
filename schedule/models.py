from django.db import models
from django.contrib.auth.models import User

class FlatMate(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    Name = models.CharField(max_length=200)
    Available = models.IntegerField(default=1, help_text="set to 0 if this mate is away")
    Contact = models.CharField(max_length=200, blank=True)
    Points = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.Name

    def is_available(self):
        return self.Available == 1

class Room(models.Model):
    Name = models.CharField(max_length=200)
    Interval = models.IntegerField(help_text="this room should be cleaned every ... days")
    Cleaner = models.ForeignKey(FlatMate, on_delete=models.SET_NULL, null=True, blank=True, help_text="this room is assigned to flatmate ...")
    Since = models.DateTimeField(help_text="since when this room is assigned to the flatmate")
    Done = models.IntegerField(default=0)
    
    def __str__(self):
        return self.Name

    def is_done(self):
        return self.Done == 1
