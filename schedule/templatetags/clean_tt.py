from django import template

register = template.Library()

@register.filter
def get(a_dict, key):
    return a_dict.get(key, "")
