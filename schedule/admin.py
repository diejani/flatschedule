from django.contrib import admin

from .models import FlatMate, Room

admin.site.register(Room)
admin.site.register(FlatMate)
