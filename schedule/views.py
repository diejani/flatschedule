from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.http import HttpResponse

from .models import Room, FlatMate
test=False

def index(req, test=test):
    """
    Display Room/FlatMate tables.
    """
    if test==True:
        return HttpResponse("<h1>Add FLatmates!</h1>")
    Rooms = Room.objects.all()
    
    now = timezone.now().date()
    timeleft = {r.id: abs((now - r.Since.date()).days - r.Interval) for r in Rooms}
    Mates = FlatMate.objects.all()
    context =  {'rooms': Rooms, 'flatmates': Mates, 'timeleft': timeleft}
    return render(req, "schedule/index.html", context)

def done(req, room_id):
    """
    Set a Room to Done.
    """
    Room_obj = get_object_or_404(Room, pk=room_id)
    Room_obj.Done = 1
    Room_obj.save()
    return index(req)

def rotate(req, room_id):
    """
    Rotate a Task thats 'Done' to the next Mate.
    """
    # map each mate to his neighbouring mate
    mates_list = [mate.id for mate in FlatMate.objects.all().only("id")]
    mates_dict = {mates_list[index]: mates_list[index+1] for index, _ in enumerate(mates_list[:-1])}
    mates_dict[mates_list[-1]] = mates_list[0]
    # TODO check that each index only occurs once
    now = timezone.now()
    Room_obj = get_object_or_404(Room, pk=room_id)
    currentmate = Room_obj.Cleaner_id 
    nextmate = mates_dict[currentmate]
    if len(FlatMate.objects.filter(Available=1)) != 0:
        Room_obj.Cleaner_id = nextmate
        Room_obj.Done = 0
        Room_obj.Since = now
        Room_obj.save()
    else:
        return index(req)  # TODO print message that all flatmates are on vacation
    if FlatMate.objects.filter(pk=nextmate).values_list("Available")[0][0] == 1:
        return index(req)
    elif len(FlatMate.objects.filter(Available)) != 0:
        return rotate(req, room_id)

def hold(req, c_id):
    """
    Make a Flatmate not available.
    """
    FlatMate.objects.filter(pk=c_id).update(Available=0)
    return index(req)

def unhold(req, c_id):
    """
    Make a Flatmate available.
    """
    FlatMate.objects.filter(pk=c_id).update(Available=1)
    return index(req)
